package com.alacrityfoundation.car;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Vanessa_CU
 */
public class DataSerializer {
	
	private File file;
	private HashMap<String, ArrayList<Double>> monitoredChannels;
	
	
	public static void main( String[] args ) {
		DataSerializer ds = new DataSerializer(new File("hello.txt"), 
				"Oil Level", 
				"Tyre Pressure", 
				"Fuel Level", 
				"Engine Temperature");
		ds.generateDoubles(10);
		ds.saveDataToFile();
	}
	
	public DataSerializer(File file, String... channels) {
		this.file = file;
		this.monitoredChannels = new HashMap<String, ArrayList<Double>>();
		this.setMonitoredChannels(channels);
	}
	
	public void setMonitoredChannels(String... channels) {
		for( String channel : channels ) {
			monitoredChannels.put(channel, new ArrayList<Double>());
		}
	}
	
	public void generateDoubles(int quantity) {
		DataGenerator generator = new DataGenerator();
		for( String channel : monitoredChannels.keySet() ) {
			this.monitoredChannels.put(channel, generator.getNRandomDoubles(quantity));
		}
	}
	
	public void saveDataToFile() {
		// overrides if file doesn't exist
		PrintWriter writer;
		try {
			writer = new PrintWriter(this.file, "UTF-8");
			for(String channel : monitoredChannels.keySet()) {
				writer.println(channel+","+mapToString(channel) );
				System.out.println(channel + ": "+ mapToString(channel));
			}
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	// pushes the values into the file
	public String mapToString(String channel) {
		return monitoredChannels.get(channel).toString();
	}
}
