package com.alacrityfoundation.car;
import java.util.ArrayList;
import java.util.Random;

/**
 * 
 */

/**
 * @author Vanessa_CU
 * Generates doubles as required
 */
public class DataGenerator {
	public ArrayList<Double> getNRandomDoubles(int quantity) {
		ArrayList<Double> listDoubles = new ArrayList<Double>();
		for( int i=0 ; i<quantity; i++ ) {
			listDoubles.add(getRandomDouble());
		}
		return listDoubles;
	}
	
	public double getRandomDouble() {
		Random random = new Random();
		return random.nextDouble();
	}
}
